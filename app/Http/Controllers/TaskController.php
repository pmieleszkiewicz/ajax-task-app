<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTask;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('list', compact('tasks'));
    }

    public function create(CreateTask $request)
    {
        $task = new Task;
        $task->name = $request->task;
        $task->save();
        return $task;
    }

    public function update(CreateTask $request)
    {
        $task = Task::find($request->id);
        $task->name = $request->task;
        $task->save();
        return $task;
    }

    public function delete(Request $request)
    {
        $task = Task::find($request->id);
        $task->delete();
        return 'Task ' . $request->id . ' deleted!';
    }
}
