<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <title>Ajax Todo App</title>
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-md-auto mr-md-auto">
                <div class="card">
                    <div class="card-header">
                        Tasks <a href="#" id="addTaskButtonModal" class="float-right" data-toggle="modal" data-target="#exampleModal"><i class="far fa-plus-square"></i></a>
                    </div>
                    <div class="card-body">
                        <ul class="list-group" id="tasksCard">
                            @foreach ($tasks as $task)
                                <li class="list-group-item task-item" data-toggle="modal" data-target="#exampleModal">{{ $task->name }}<input type="hidden" id="taskId" value="{{ $task->id }}"></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitle">Add a new task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="hidden" id="id">
                            <input type="text" class="form-control" id="taskDescription" placeholder="Task description">
                        </div>
                        <div class="float-right">
                            <button id="deleteButton" class="btn btn-danger hidden" data-dismiss="modal">Delete</button>
                            <button id="saveButton" class="btn btn-success hidden" data-dismiss="modal">Save</button>
                            <button id="addButton" class="btn btn-success" data-dismiss="modal">Add task</button>
                            {{ csrf_field() }}
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>