$(document).ready(function() {
    $(document).on('click', '.task-item', function(event) {
        event.preventDefault();
        const text = $(this).text();
        const id = $(this).find('#taskId').val();
        $('#taskDescription').val(text);
        $('#modalTitle').text('Edit task');
        $('#deleteButton').removeClass('hidden');
        $('#saveButton').removeClass('hidden');
        $('#addButton').hide();
        $('#id').val(id);
        console.log(id);
    });


    $(document).on('click', '#addTaskButtonModal', function(event) {
        $('#taskDescription').val('');
        $('#modalTitle').text('Add task');
        $('#deleteButton').addClass('hidden');
        $('#saveButton').addClass('hidden');
        $('#addButton').show();
    });


   $('#addButton').click(function(event) {
       event.preventDefault();
       const task = $('#taskDescription').val();

       $.post("task", { 'task': task, '_token': $('input[name=_token]').val() }, function(data) {
           console.log(data);
           $('#tasksCard').load(location.href + ' #tasksCard');
       });
   });

    $('#deleteButton').click(function(event) {
        event.preventDefault();
        const id = $('#id').val();

        $.post("task-delete", { 'id': id, '_token': $('input[name=_token]').val() }, function(data) {
            console.log(data);
            $('#tasksCard').load(location.href + ' #tasksCard');
        });
    });

    $('#saveButton').click(function(event) {
        event.preventDefault();
        const id = $('#id').val();
        const task = $('#taskDescription').val();

        $.post("task-edit", { 'id': id, 'task': task, '_token': $('input[name=_token]').val() }, function(data) {
            console.log(data);
            $('#tasksCard').load(location.href + ' #tasksCard');
        });
    });
});